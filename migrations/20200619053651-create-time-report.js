'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('time_reports', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      recordId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: "records",
          key: "id"
        }
      },
      date: {
        type: Sequelize.DATEONLY,
        allowNull: false
      },
      hoursWorked: {
        type: Sequelize.DECIMAL(5,2),
        allowNull: false
      },
      amountPaid: {
        type: Sequelize.DECIMAL(10,2),
        allowNull: false
      },
      employeeId: {
        type: Sequelize.STRING,
        allowNull: false
      },
      jobGroupId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: "job_groups",
          key: "id"
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('time_reports');
  }
};