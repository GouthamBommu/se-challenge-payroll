import GraphQL from './utils/graphql'
import { typeDefs as reportDefs, resolvers as reportResolvers} from './modules/report/report.schema'
import { typeDefs as jobGroupDefs, resolvers as jobGroupResolvers } from './modules/job-group/job-group.schema'
import { typeDefs as recordDefs, resolvers as recordResolvers} from './modules/record/record.schema'

export default class Main {

    private graphql = new GraphQL()
    constructor() {
        // GraphQL Server
        this.graphql.addToSchema(reportDefs, reportResolvers)
        this.graphql.addToSchema(jobGroupDefs, jobGroupResolvers)
        this.graphql.addToSchema(recordDefs, recordResolvers)
        
        this.graphql.start()
    }
}
const main = new Main()
