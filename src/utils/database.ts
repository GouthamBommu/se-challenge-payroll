const { Sequelize } = require('sequelize');
import JobGroup from "../models/job-group"
import Record from "../models/record"
import TimeReport from "../models/time-report"

const sequelize = new Sequelize(
    process.env.MYSQL_DATABASE, 
    process.env.MYSQL_USER, 
    process.env.MYSQL_PASSWORD, 
    {
        host: process.env.MYSQL_HOST,
        dialect: 'mysql'
    },
)

const models = {
    JobGroup: JobGroup.init(sequelize, Sequelize),
    Record: Record.init(sequelize, Sequelize),
    TimeReport: TimeReport.init(sequelize, Sequelize)
};

// Run `.associate` if it exists,
// ie create relationships in the ORM
Object.values(models)
  .filter(model => typeof model.associate === "function")
  .forEach(model => model.associate(models));

const db = {
  ...models,
  sequelize
};
export default db