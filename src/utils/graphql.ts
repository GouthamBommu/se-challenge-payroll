const { buildFederatedSchema } = require('@apollo/federation')
import { attachDirectiveResolvers } from 'graphql-tools'
import gql from 'graphql-tag'
import { merge } from 'lodash'

export default class GraphQL {

    private PORT = process.env.PORT || 3000
    private typeDefs = gql`
        directive @auth on FIELD_DEFINITION
    `
    private resolvers = {}

    public start(app = undefined) {
        let [typeDefs, resolvers] = [this.typeDefs, this.resolvers]

        const schema = buildFederatedSchema([
            {
                typeDefs,
                resolvers,
            },
        ])

        const directiveResolvers = {}

        attachDirectiveResolvers(schema, directiveResolvers)
        let serverConfig = {
            debug: false,
            schema
        }

        const { ApolloServer } = require('apollo-server')
        let server = new ApolloServer(serverConfig)
        server.listen({ port: this.PORT }).then(({ url }) => {
            console.log(`🚀  Server ready at ${url}`)
        })
    }

    public addToSchema(typeDefsIn, resolversIn) {
        this.typeDefs = gql`
            ${this.typeDefs}
            ${typeDefsIn}
        `
        merge(this.resolvers, resolversIn)
    }
}
