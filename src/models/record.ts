const { Model } = require('sequelize');

export default class Record extends Model {
    static init(sequelize, DataTypes){
        return super.init(
            {
              recordId: {
                type: DataTypes.INTEGER,
                unique: true,
                allowNull: false
              }
            }, 
            {   sequelize,
                modelName: 'record',
                timestamps: true
            }
        )
    }
}