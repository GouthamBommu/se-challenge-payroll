const { Model } = require('sequelize');

export default class JobGroup extends Model {
    static init(sequelize, DataTypes){
        return super.init(
            {
              name: {
                type: DataTypes.STRING,
                unique: true,
                allowNull: false
              },
              hourlyPay: {
                type: DataTypes.DECIMAL,
                allowNull: false
              }
            }, 
            {   sequelize,
                modelName: 'job_group',
                timestamps: true
            }
        )
    }
}