const { Model } = require('sequelize');

export default class TimeReport extends Model {
    static init(sequelize, DataTypes){
        return super.init(
            {
              recordId: {
                type: DataTypes.INTEGER,
                allowNull: false
              },
              date: {
                type: DataTypes.DATEONLY,
                allowNull: false
              },
              hoursWorked: {
                type: DataTypes.DECIMAL(5,2),
                allowNull: false
              },
              amountPaid: {
                type: DataTypes.DECIMAL(10,2),
                allowNull: false
              },
              employeeId: {
                type: DataTypes.STRING,
                allowNull: false
              },
              jobGroupId: {
                type: DataTypes.INTEGER,
                allowNull: false
              },
            }, 
            {   sequelize,
                modelName: 'time_report',
                timestamps: true
            }
        )
    }

    static associate(models){
      // this.hasMany(models.Record)
      // models.Record.belongsTo(this)
      // this.hasMany(models.JobGroup)
      // models.JobGroup.belongsTo(this)

      /////////////////////
      this.belongsTo(models.Record)
      models.Record.hasMany(this)

      this.belongsTo(models.JobGroup)
      models.JobGroup.hasMany(this)
    }
}