import gql from 'graphql-tag'
import { GraphQLResolverMap } from '@apollographql/apollo-tools'
import Record from './record'
const record = new Record()

export const typeDefs = gql`
    scalar Upload
    input RecordInput {
        recordId: ID
    }

    type Record {
        id: ID
        recordId: ID
    }

    extend type Query {
        getRecords: [Record]
    }

    extend type Mutation {
        createRecord(input: RecordInput!): Record 
        updateRecord(id: ID!,input: RecordInput!): Boolean
        deleteRecord(id: ID!): Boolean
        uploadFile(file: Upload!): Boolean
    }
`

export const resolvers: GraphQLResolverMap<any> = {

    Query: {
        getRecords: async () => await record.get(),
    },

    Mutation: {
        createRecord: async (_, { input }) => await record.create(input),
        updateRecord: async (_, { id, input }) => await record.update(id, input),
        deleteRecord: async (_, { id }) => await record.delete(id),
        uploadFile: async(_, {file}) => await record.upload(file)
    }
}