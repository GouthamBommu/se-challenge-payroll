import Database from "@utils/database"
const record = Database.Record
import File from "../file/file"
const seq = Database.sequelize
const fs = require("fs")
import Report from "../report/report"

export default class RECORD {

    private file = new File()
    private report = new Report()

    public async upload(record){

        try{
            const {createReadStream, filename, mimetype} = await record

            if(mimetype.search(/csv$/g) != -1){
                
                let fileName = filename
                const recordId = fileName.replace(/\.\w+$/g, "").split("-").pop()
                let recordInfo = await this.getByRecordId(recordId)
                
                if(!recordInfo){
                    const fileStream = createReadStream()
                    fileStream.pipe(fs.createWriteStream(`./${filename}`))

                    const result = await seq.transaction(async(t) => {

                            let newRecord = await this.create({recordId: recordId}, {transaction: t})

                            /* READ CSV FILE*/
                            const timeReps = await this.file.read(fileName, newRecord)

                            /*INSERT REPORT DATA */
                            await this.report.bulkCreate(timeReps, {transaction: t})

                            return true
                    })

                    return result
                }
                else{
                    return new Error("This record is already uploaded")
                }
            }
            else{
                return new Error("Invalid file type")
            }
        }
        catch(error){
            console.log(error)
            throw new Error("upload failed")
        }
    }

    public async get(id=undefined){
        const condition = id ? {
            where:{
                id:id 
            }
        } : {} 
  
        return id ? await record.findOne(condition): await record.findAll()
    }
  
    public async create(input, options={}){
          const rec = await record.create(input,options)
          return rec
    }
  
    public async update(id, input){
        const response = await record.update(input, {where: {id: id}})
        return response[0]
    }
  
    public async delete(id){
        const response = await record.destroy({where: {id: id}})
        return response
    }

    public async getByRecordId(recordId){
        return await record.findOne({where: {recordId: recordId}})
    }
}