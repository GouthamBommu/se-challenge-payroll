const path = require('path')
const csv = require("csvtojson");
const moment = require('moment')
import JobGroup from "../job-group/job-group"

export default class File{
    
    private jobGroup = new JobGroup()

    public async read(name, newRecord){

        let timeReps = []
        let fileAbsPath = path.resolve(`./${name}`)
        let dailyReports = await csv()
                                .fromFile(fileAbsPath)
        const recordId = name.replace(/\.\w+$/g, "").split("-").pop()
                        
        for(let dailyReport of dailyReports){

            let jobGroup = dailyReport['job group']
            let jobGroupInfo = await this.jobGroup.getByJobGroup(jobGroup).catch(error => {
                throw new Error("Failed to get Job group")
            })

            let timeRep = {
                recordId: newRecord["id"],
                date: moment(dailyReport["date"], "D/M/YYYY").format("YYYY-MM-DD"),
                hoursWorked: dailyReport['hours worked'],
                amountPaid: dailyReport['hours worked'] * jobGroupInfo["hourlyPay"],
                employeeId: dailyReport['employee id'],
                jobGroupId: jobGroupInfo["id"]
            }

            timeReps.push(timeRep)
        }

        return timeReps
    }
}