const moment = require('moment')
const { QueryTypes } = require('sequelize');
import Database from "@utils/database"
const timeReport = Database.TimeReport
const seq = Database.sequelize

export default class Report {

    public async getPayrollReport(){

        let result = {
            payrollReport: {
                employeeReports: []
            }
        }
        let query = `SELECT 
                        employeeId, 
                        YEAR(date) AS year, 
                        CEIL(DayOfYear(date)/15) AS biweek, 
                        SUM(amountPaid) AS amountPaid
                    FROM time_reports
                    group by employeeId, YEAR(date), CEIL(DayOfYear(date)/15)
                    order by employeeId, YEAR(date), CEIL(DayOfYear(date)/15)`


        let records = await seq.query(query, { type: QueryTypes.SELECT });
        let payrolls = []

        for(let record of records){
            let payPeriod = await this.getPayPeriod(record.year, record.biweek)
            let payroll = {
                employeeId: record.employeeId,
                payPeriod: payPeriod,
                amountPaid: "$"+record.amountPaid
            }
            payrolls.push(payroll) 
        }

        result.payrollReport.employeeReports = payrolls
        return result
    }

    private async getPayPeriod(year, biweek){
        let month = Math.ceil(biweek/2)
        let startDate = biweek%2 != 0 ? [year, month,"01"].join("-") : [year, month,"16"].join("-")
        let dayOfStartDate = moment(startDate, "YYYY-MM-DD").format("D")
        let daysInMonth = moment(`${year}`+ "-"+ `${month}`, "YYYY-M").daysInMonth() 
        let endDate = dayOfStartDate == 1 ? [year, month,"15"].join("-") : [year, month,daysInMonth].join("-")
        return {startDate: startDate, endDate: endDate}
    }

    public async get(id=undefined){
        const condition = id ? {
            where:{
                id:id 
            }
        } : {} 
  
        return id ? await timeReport.findOne(condition): await timeReport.findAll()
    }
  
    public async create(input, options={}){
          const rep = await timeReport.create(input, options)
          return rep
    }
  
    public async update(id, input){
        const response = await timeReport.update(input, {where: {id: id}})
        return response[0]
    }
  
    public async delete(id){
        const response = await timeReport.destroy({where: {id: id}})
        return response
    }

    public async getByJobGroup(groupName){
        return await timeReport.findOne({where: {name:groupName}})
    }

    public async bulkCreate(reports, options={}){
        const reps = await timeReport.bulkCreate(reports, options)
        return reps
    }
}