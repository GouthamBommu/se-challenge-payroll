import gql from 'graphql-tag'
import { GraphQLResolverMap } from '@apollographql/apollo-tools'
import Report from "./report"
const report = new Report()

export const typeDefs = gql`
    type payPeriod {
        startDate: String
        endDate: String
    }

    type employeeReport {
        employeeId: ID
        payPeriod: payPeriod
        amountPaid: String
    }

    type employeeRecords {
        employeeReports: [employeeReport]
    }

    type payrollReport {
        payrollReport: employeeRecords
    }

    extend type Query {
        getPayrollReport: payrollReport
    }
`

export const resolvers: GraphQLResolverMap<any> = {

    Query: {
        getPayrollReport: async() => await report.getPayrollReport()
    }
}