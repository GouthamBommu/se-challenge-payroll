import gql from 'graphql-tag'
import { GraphQLResolverMap } from '@apollographql/apollo-tools'
import JobGroup from './job-group'
const jobGroup = new JobGroup()

export const typeDefs = gql`
    input JobGroupInput {
        name: String
        hourlyPay: String
    }

    type JobGroup {
        id: ID
        name: String
        hourlyPay: String
    }

    extend type Query {
        getJobGroups: [JobGroup]
    }

    extend type Mutation {
        createJobGroup(input: JobGroupInput!): JobGroup 
        updateJobGroup(id: ID!,input: JobGroupInput!): Boolean
        deleteJobGroup(id: ID!): Boolean
    }
`

export const resolvers: GraphQLResolverMap<any> = {

    Query: {
        getJobGroups: async () => await jobGroup.get(),
    },

    Mutation: {
        createJobGroup: async (_, { input }) => await jobGroup.create(input),
        updateJobGroup: async (_, { id, input }) => await jobGroup.update(id, input),
        deleteJobGroup: async (_, { id }) => await jobGroup.delete(id),
    }
}