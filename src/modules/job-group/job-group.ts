import Database from "@utils/database"
const jobGroup = Database.JobGroup

export default class JobGroup {
    public async get(id=undefined){
        const condition = id ? {
            where:{
                id:id 
            }
        } : {} 
  
        return id ? await jobGroup.findOne(condition): await jobGroup.findAll()
    }
  
    public async create(input, options={}){
          const group = await jobGroup.create(input, options)
          return group
    }
  
    public async update(id, input){
        const response = await jobGroup.update(input, {where: {id: id}})
        return response[0]
    }
  
    public async delete(id){
        const response = await jobGroup.destroy({where: {id: id}})
        return response
    }

    public async getByJobGroup(groupName){
        return await jobGroup.findOne({where: {name:groupName}})
    }
}