const path = require('path');
const { NODE_ENV } = process.env

var nodeModules = {};
nodeModules['apollo-server'] = 'commonjs apollo-server';
nodeModules['@apollo/federation'] = 'commonjs @apollo/federation';
nodeModules['graphql-tools'] = 'commonjs graphql-tools';
nodeModules['graphql-tag'] = 'commonjs graphql-tag';
nodeModules['mysql2/promise'] = 'commonjs mysql2/promise';

module.exports = {
    entry: './src/index.ts',
    mode: NODE_ENV,
    target: 'node',
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'index.js'
    },
    externals: nodeModules,
    resolve: {
        mainFields: ['browser', 'main', 'module'],
        extensions: ['.ts', '.js'],
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: [
                    'ts-loader',
                ]
            }
        ]
    }
}
