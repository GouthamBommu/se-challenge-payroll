'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('job_groups', [{
      name: 'A',
      hourlyPay: 20,
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      name: 'B',
      hourlyPay: 30,
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('job_groups', null, {});
  }
};
